<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    $x = 12;
                    $y = 20;
                    function sum(){
                        $GLOBALS['z'] = $GLOBALS['x'] + $GLOBALS['y'];//here we are using $GLOBALS instead of global to declare a variable as a global scope.
                    }
                    sum();
                    echo $z."<br>";
                    print_r($_SERVER['PHP_SELF']);//by this code we can see the path of our file.
                    echo '<br>';
                    echo '<pre>';
                    print_r($_SERVER);
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

