<?php
    include './header.php';
?>
            <div class="maincontent">
                
                <span style="float:right">
                <?php
                    date_default_timezone_set('Asia/Dhaka')."<br>";//we are setting the time zone here.
                    echo "Local Time is ".date("h:i:sa"); 
                ?>
                </span>
                
                <?php
                    
                    echo "Today is ".date("m/d/y")."<br>";//this code is to show the date.
                    echo "Today is ".date("l")."<br>";//this code is to show the name of the day.
                    echo "Default Time is ".date("h:i:sa")."<br>";//this code is to show the time without time zone.
                    
                    //echo date_default_timezone_get();//we are getting the timezone dynamically.
                ?>
            </div>
                  
<?php
                include  './footer.php'; 
?>