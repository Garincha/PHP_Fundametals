<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    $a = 5;
                    echo $a++;//its a post increment example. here it will print the value 5.
                    echo '<br>';
                    echo $a;// now it will print the value 6 by the post increment operator.
                    echo '<br>';
                    $b = 8;
                    echo ++$b;//its a pre increment operator. here it will print the value 9. cause it will first increment the value then print.
                    echo '<br>';
                    $r = 4;
                    echo $r--;//post decrement operator.here it will print the value 4
                    echo '<br>';
                    echo $r;//now it will print the value 3 after decrement.
                    echo '<br>';
                    $s = 5;
                    echo --$s;//now it will print 4 by pre decrement operator.
                    
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


