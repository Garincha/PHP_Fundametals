<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    $a = 33; $b = 59;
                    if($a == 33 && $b == 59){
                        echo 'True';
                    }  else {
                           echo 'false';
                            }
                            echo '<br>';
                    if($a == 33 || $b == 59){
                        echo 'Right';
                    }  else {
                        echo 'Wrong';
                    }
                    echo '<br>';
                    if($a == 33 xor $b == 58){
                        echo 'Yes';
                    }  else {
                        echo 'No';
                    }
                    echo '<br>';
                    if($a !== 32){
                        echo 'It is not equal to 32';
                    }
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

