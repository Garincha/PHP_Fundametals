<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    echo 'Example of indexed array';
                    $a = array(2,83,949,3738,0);
                    echo '<pre>';
                    print_r($a);
                    echo '<br>';
                    echo 'Counting the length of an array ';
                    echo count($a);//by count function we can count the length of an array.
                    echo '<br>';
                    $length = count($a);
                    for($i = 0;$i < $length;$i++){//here $i is working as a counter.
                        echo "$a[$i] <br>";
                    }
                    echo 'Example of Associative Array';
                    echo '<br>';
                    $b = array('one'=>'USA','two'=>'USSR','three'=>'UK');
                    $len = count($b);
                    foreach ($b as $key=>$value){//all keys of associative array will go to $key and all value will go to $value.
                        echo '<pre>';
                        echo "Key = ".$key.",Value = ".$value;
                        
                    }
                    echo '<br>';
                    echo 'Example of Multidimentional Array';
                    echo '<br>';
                        $f = array(
                            array("BMW",23,56),
                            array("Volvo",90,0.9),
                            array("Toyota",45,89)
                        );
                        echo $f[0][0 ],$f[0][2];
                        echo '<br>';
                        echo $f[1][0 ],$f[1][2];
                        echo '<br>';
                        echo $f[2][0 ],$f[0][2];
                        echo '<br>';
                        for($row = 0;$row < 3;$row++){
                            echo "<p>Row number is : $row</p>";
                            echo '<ul>';
                            for($col = 0;$col < 3;$col++){
                                echo "<li>".$f[$row][$col]."</li>";
                            }
                            echo '</ul>';
                        }
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

