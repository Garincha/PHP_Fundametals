<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    function zmd($x){//here $x is a parameter/argument by which we can pass a value
                        echo "I am drilling PHP".$x;
                    }
                    zmd(" i love it");//we passing value from here.
                    echo '<br>';
                    function school($x,$y){
                        echo "The best school is $x ,$y <br>";
                    }
                    school("KPL","1867");
                    school("SMG","1898");
                    school("LMP","1945");
                    echo '<br>';
                    function college($a = "good"){
                        echo "Our college is $a <br>";
                    }
                    college("number 1");
                    college("number 2");
                    college("number 3");
                    college();//this is empty & by default the "good" will show here.
                    echo '<br>';
                    function exe($x = "",$y = ""){
                        $z = $x+$y;
                        echo $z;
                    }
                    exe(5,10);
                    
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


