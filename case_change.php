<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
            input[type="text"]{width:250px;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    
                    if(isset($_POST['text'])){
                        $store = $_POST['text'];//here we are assigning the value the variable $store,which we got through the $_post['text].
                        echo strtoupper($store);//we are converting all letters to upper case.
                    }
                    if(isset($_POST['text1'])){
                        $str = $_POST['text1'];
                        echo strtolower($str);//we are converting all letters to lower case.
                    }
                    if(isset($_POST['text2'])){
                        $str2 = $_POST['text2'];
                        echo ucfirst($str2);//we converting the firtst letter of the sentence in capital letter.
                    }
                    if(isset($_POST['text3'])){
                        $str3 = $_POST['text3'];
                        echo ucwords($str3);//we are converting the all first letters of the words into capital letters of a sentence.
                    }
                ?>
                <form method="post" action="">
                    <input type="text" name="text" value="<?php global $store;  echo $store;//to retain the value of $store,written this code?>">
                    <br>
                    <br>
                    <input type="text" name="text1" value="<?php global $str;  echo $str;//to retain the value of $store,written this code?>">
                    <br>
                    <br>
                    <input type="text" name="text2" value="<?php global $str2;  echo $str2;//to retain the value of $store,written this code?>">
                    <br>
                    <br>
                    <input type="text" name="text3" value="<?php global $str3;  echo $str3;//to retain the value of $store,written this code?>">
                    <br>
                    <br>
                    <input type="submit" value="Submit">
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


