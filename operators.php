<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                1.Arithmetic Operator
                <br>
                2.Assignment Operator
                <br>
                3.Comparison Operator
                <br>
                4.Increment/Decrement Operator
                <br>
                5.Logical Operator
                <br>
                6.String Operator
                <br>
                7.Array Operator
                <br>
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    $a = 4;$b = 4;
                    echo $z = $a + $b;//example of arithmetic operator
                    echo '<br>';
                    $x = 100; $y = 100;
                    var_dump($x == $y);//by var_dump we are getting a boolean value. it is a example of comparision operator.here == checks only the value of both variable.
                    echo '<br>';
                    $d = 234;$e = "234";
                    var_dump($d === $e);//comparision operator. here === checks the value and data type of both variable.
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>
