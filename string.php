<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    $s = "I am a PHP Developer";
                    echo 'The string length is '.strlen($s);//here we are counting the total string length of the variable $s.
                    echo '<br>';
                    $a = "Bangls Desh";
                    echo 'The word count is '.  str_word_count($a);//here we are counting the words within the $a variable.
                    echo '<br>';
                    $b = "watch out";
                    echo 'The reverse mode is '.  strrev($b);//here we are reversing string within $b.
                    echo '<br>';
                    $g = "php is best";
                    echo 'The position of the word best is '.  strpos($g, "best");//here we are indicating the position the stirng best.
                    echo '<br>';
                    $d = "PHP is no 1";
                    echo 'We are replacing the word PHP by Java so '.  str_replace('PHP','Java', $d);
                    echo '<br>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

