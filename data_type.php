<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $a = "This is a string";
                    echo $a;
                    echo '<br>';
                    $b = 3;
                    echo $b.' This is a integer';
                    echo '<br>';
                    $c = 49.44;
                    echo $c.' This is a float';
                    echo '<br>';
                    $d = TRUE;
                    var_dump($d);
                    echo '<br>';
                    $e = FALSE;
                    var_dump($e);
                    echo '<br>';
                    $f = array('garincha','best','dribller');
                    var_dump($f);
                    echo '<br>';
                    class student{
                        function subject(){
                            return 'Physics(this is a part of class and coming here through a object)';
                        }
                        function details(){
                            echo $this->subject();
                        }
                    }
                    $str = new student();
                    $str->details();
                    echo '<pre>';
                    var_dump($str);
                    echo '<br>';
                    $g = NULL;
                    var_dump($g);
                    echo '<br>';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


