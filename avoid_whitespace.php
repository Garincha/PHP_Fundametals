<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    if(isset($_POST['txt'])){
                       $mytxt = $_POST['txt'];
                       echo trim($mytxt," ");
                    }
                    if(isset($_POST['pw'])){
                        $sec = $_POST['pw'];
                        echo trim($sec," ");
                    }
                ?>
                <form action="" method="post">
                    <input type="text" name="txt" value="<?php  global $mytxt;echo $mytxt;?>">
                    <br>
                    <br>
                    <input type="password" name="pw">
                    <br>
                    <br>
                    <input type="submit" value="Submit">
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


