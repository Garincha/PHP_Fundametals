<?php

?>

<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $num = 300;
                    $min = 1;
                    $max = 200;
                    if(filter_var($num,FILTER_VALIDATE_INT,array("options"=>
                        array("min_range" => $min,"max_range" => $max )))){
                            echo 'It is valid range';
                    }else {
                        echo "It is not valid range";
                    }
                    echo '<br>';
                    $url = "http://www.google.com/index.html?q=123";
                    if(filter_var($url,FILTER_VALIDATE_URL,FILTER_FLAG_QUERY_REQUIRED)){//this FILTER_FLAG_QUERY_REQUIRED function ensures the type of url in $url.
                        echo 'This URL is valid';
                    }  else {
                        echo 'This URL is not valid';
                    }
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


