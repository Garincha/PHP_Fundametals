<?php

?>

<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor'."<br>";
                ?>
                <!--------- >
                <table>
                    <tr>
                        <td>Filter Name</td>
                        <td>Filter Id</td>
                    </tr>
                    <?php
                        //foreach (filter_list() as $id=>$filter){
                            //echo '<tr><td>'.$filter.'</td><td>'.  filter_id($filter).'</td></tr>';
                        //}
                    ?>
                </table>
                <!---->
                <?php
                    $str = "<h2>I am a PHP Developer</h2>"."<br>";
                    echo $str;
                    $str2 = filter_var($str,FILTER_SANITIZE_STRING);//here we filtering(sanitization) the h2 tag by this code.
                    echo $str2."<br>";
                    $int = 44.09;
                    if(filter_var($int,FILTER_VALIDATE_INT)){
                        echo 'This is an integer'."<br>";
                    }else {
                        echo 'This is not an integer'."<br>";
                    }
                    $ip = "127.0.0.1";
                    if(filter_var($ip,FILTER_VALIDATE_IP)){
                        echo 'This a IP Address'."<br>";
                    }  else {
                        echo 'This is not a IP Address'."<br>";
                    }
                    $email = "johndesh@yahoo.com";
                    if(filter_var($email,FILTER_VALIDATE_EMAIL)){
                        echo "$email This is a valid email address"."<br>";
                    }  else {
                        echo "$email This is not a valid email address"."<br>";
                    }
                    $url = "http://www.google.com";
                    if(filter_var($url,FILTER_VALIDATE_URL)){
                        echo "$url this is a valid url";
                    }  else {
                        echo "$url this is not a valid url";
                    }
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

