<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor'."<br>";
                    $file_create = fopen("new.txt","w") or die('File not found.');//here, we are creating a new file named new.txt
                    $one = "Hello Everyone.";
                    fwrite($file_create, $one);//now,here we the value of $one in new.txt file
                    $two = "Welcome to this PHP World.";
                    fwrite($file_create, $two);//now,here we the value of $one in new.txt file
                    fclose($file_create);
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


