<?php
    setcookie('Visited',"",  time()-3600);//to destroy the cookie we write this line.
?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php//this code is for to create a cookie
                    //if(!isset($_COOKIE['Visited'])){//if the $_cookie['visited'] is not set, then it will execute the code blocks below
                        //setcookie("Visited","1",  time()+86400,"/") or die('Could not set the cookie');//86400 means the total seconds in 24 hours.
                        //echo 'This is your first visit in this site';
                    //}  else {
                        //echo 'This is not your first visit.';
                    //}
                    
                ?>
                <?php
                    echo 'Cookies has been deleted.';
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

