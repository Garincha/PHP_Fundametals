<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                    User Name: <input type="text" name="username">
                    <input type="submit" value="submit">
                </form>
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    if($_SERVER['REQUEST_METHOD']== 'POST'){
                        $name=$_REQUEST['username'];//first,we are getting the value through $_REQUEST, now we are keeping the value in $name. 
                        if(empty($name)){
                            echo '<span style = "color:red" >User Name is Required</span>'; 
                        }  else {
                            echo "<span style = 'color:green'>You have entered User Name: .$name.</span>";
                        }
                    }
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

