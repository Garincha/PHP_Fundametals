<?php

?>

<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    $ourfile = fopen('best.txt','r') or die('File not found.');//here we opens the file in read mode and keeps in a variable.
                    //echo fread($ourfile,  filesize('best.txt'))."<br>";//here in 1st parameter the open file exists, in 2nd parameter, filesize will show the maximum byte of the file.
                    //echo fgets($ourfile,  filesize('best.txt'))."<br>";;
                    //echo fgetc($ourfile)."<br>";
                    while (!feof($ourfile)){
                        echo fgets($ourfile)."<br>";//by this whole code, it reads the whole file line by line.
                    }
                    echo '<br>';
                    while (!feof($ourfile)){
                        echo fgetc($ourfile)."<br>";//by this whole code, it reads the every word seperately.
                    }
                    fclose($ourfile);
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>
