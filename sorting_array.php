<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    $name = array('Boby','Abraham','Danny','Cameron');
                    sort($name);//by sort function we are sorting the values & keys of array alphabetically(ascending)
                    echo '<pre>';
                    print_r($name);
                    echo '<br>';
                    $name = array('Boby','Abraham','Danny','Cameron');
                    rsort($name);//by rsort(reverse sort) function we are sorting the values & keys of array oppositely.(descending)
                    echo '<pre>';
                    print_r($name);
                    echo '<br>';
                    $num = array(57,95,2,34,100,0,0.9,-0.88);
                    $length = count($num);
                    sort($num);//here, by sort function we are sorting all integer from 0.(ascending order)
                    for($i = 0;$i < $length;$i++){
                        echo "$num[$i]<br>";
                    }
                    echo '<br>';
                    $num = array(57,95,2,34,100,0,0.9,-0.88);
                    $length = count($num);
                    rsort($num);//here, by rsort(reverse sort) function we are sorting all integer from the highest form.(descending)
                    for($i = 0;$i < $length;$i++){
                        echo "$num[$i]<br>";
                    }
                    echo '<br>';
                    echo 'Sort by asort function(ascending sort) ';
                    $cam = array('sam' => '68','Rom'=>'33','Danny'=>'12');
                    asort($cam);//by asort function we sorting values with ascending order.
                    echo '<pre>';
                    print_r($cam);
                    echo '<br>';
                    echo 'Sort by ksort function(ascending sort for alphabetic order) ';
                    $kam = array('Zaman' => '123','Rohim'=>'33','Arman'=>'100');
                    ksort($kam);//by ksort function we sorting keys with ascending order.
                    echo '<pre>';
                    print_r($kam);
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

