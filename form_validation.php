<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Form Validation';
                ?>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);//in action we are using this code to take action within this page or we can leave the place empty.to protect hacking we are using htmlspecialchars?>">
                <table>
                    <tr>
                        <td>Name :</td>
                        <td><input type="text" name="user_name" required="1"></td><!--required means this field must be fulfilled -->
                    </tr>
                    <tr>
                        <td>Email :</td>
                        <td><input type="text" name="user_email" required="1"></td>
                    </tr>
                    <tr>
                        <td>Website :</td>
                        <td><input type="text" name="website" required="1"></td>
                    </tr>
                    <tr>
                        <td>Comments :</td>
                        <td><textarea name="comment" rows="5" cols="50"></textarea></td>
                    </tr>
                    <tr>
                        <td>Gender :</td>
                        <td>
                        <input type="radio" name="gender" value="male">Male
                        <input type="radio" name="gender" value="female">female
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="submit" value="Submit"></td>
                    </tr>
                </table>
                </form>
                <?php 
                    if($_SERVER["REQUEST_METHOD"] == "POST"){//here, by this code we are ensuring that all request/data comes through post method.
                        $Name  = validate($_POST['user_name']);//getting the data through the key user_name and kepping in $name variable.
                        $Mail  = validate($_POST['user_email']);//getting the data through the key user_email and kepping in $mail variable.
                        $Web   = validate($_POST['website']);//getting the data through the key website and kepping in $web variable.
                        $Com   = validate($_POST['comment']);//getting the data through the key comment and kepping in $com variable.
                        $Gen   = validate($_POST['gender']);//getting the data through the key gender and kepping in $gen variable.
                        $Sub   = validate($_POST['submit']);//getting the data through the key submit and kepping in $sub variable.
                        
                        echo "Name : ".$Name."<br>";
                        echo "Mail : ".$Mail."<br>";
                        echo "Website : ".$Web."<br>";
                        echo "Comment : ".$Com."<br>";
                        echo "Gender : ".$Gen."<br>";
                        echo "Submit : ".$Sub;
                    }
                    
                    function validate($data){//data(exe-$_post(user_name)) first comes to this parameter $data.by this function we are validating our data.
                        $data1 = trim($data);//then it goes through trim function and stores in $data1.
                        $data2 = stripcslashes($data1);//then it goes through stripclashed function and stores in $data2
                        $data3 = htmlspecialchars($data2);//then it goes through htmlspecialchars function and stores in $data3
                        return $data3;
                    }
                    
                    
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


