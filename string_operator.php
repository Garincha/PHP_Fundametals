<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    echo 'PHP Hypertext Pre Processor';
                    echo '<br>';
                    $a = "PHP its former name was";
                    $v = " Personal Home Page";
                    echo $a.$v;
                    echo '<br>';
                    $a .= $v;
                    echo $a;//now here we can see the value of both variable $a & $v.cause by .= string operator we assigned the value of $v within $a.
                ?>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>


