<?php

?>
<html>
    <head>
        <title>PHP Syntax</title>
        <style>
            .code{width:900px; margin: 0 auto; background:<?php echo '#ddd';?>;text-align: center;}
            .headeroption,.footeroptions{background: #444;color: #fff;text-align: center;padding: 20px;}
            .maincontent{min-height: 300px;padding: 20px;}
            .headeroption h2,.footeroptions h2{margin: 0;}
        </style>
        
    </head>
    
    <body>
        <div class="code">
            <div class="headeroption">
                <h2><?php echo 'PHP Fundamentals'?></h2>
            </div>
            <div class="maincontent">
                <?php
                    if(isset($_FILES['image'])){//we are doing a validation here
                        $filename = $_FILES['image']['name'];//this code is for file name form client side
                        $filetmp = $_FILES['image']['tmp_name'];// by this code we can upload a temporary copy of the image file.
                        move_uploaded_file($filetmp,"images/".$filename);//we are uploading that image file in a folder named images.
                        echo "Image uploaded succesfully";
                    }
                ?>
                <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
                    <input type="file" name="image">
                    <input type="submit" value="Submit">
                </form>
            </div>
                  
            <div class="footeroptions">
                <h2><?php echo 'www.w3schools.com';?></h2>
            </div>
            
        </div>
       
    </body>
</html>

